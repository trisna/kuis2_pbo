
//proses menggunakan integer
void prosesint(int pembilang1, int penyebut1, int pembilang2, int penyebut2){
	int tamppembilang1, tamppembilang2;
	int tamppenyebut = penyebut1*penyebut2;

	cout<<"Hasil menggunakan integer\n";
	cout<<"Hasil perkalian :\n";
	cout<<pembilang1*pembilang2;
	cout<< "/";
	cout<<penyebut1*penyebut2;

	tamppembilang1 = (tamppenyebut/penyebut1)*pembilang1; 
	tamppembilang2 = (tamppenyebut/penyebut2)*pembilang2;

	cout<<"\n";
	cout<<"Hasil Pertambahan :\n";
	cout<<tamppembilang1+tamppembilang1;
	cout<<"/";
	cout<<tamppenyebut;

}

//proses menggunakan double
void prosesdouble(double pembilang1, double penyebut1, double pembilang2, double penyebut2){
	double tamppembilang1, tamppembilang2;
	double tamppenyebut = penyebut1*penyebut2;

	cout<<"\nHasil menggunakan double\n";
	cout<<"Hasil perkalian :\n";
	cout<<pembilang1*pembilang2;
	cout<< "/";
	cout<<penyebut1*penyebut2;

	tamppembilang1 = (tamppenyebut/penyebut1)*pembilang1; 
	tamppembilang2 = (tamppenyebut/penyebut2)*pembilang2;

	cout<<"\n";
	cout<<"Hasil Pertambahan :\n";
	cout<<tamppembilang1+tamppembilang1;
	cout<<"/";
	cout<<tamppenyebut;

}
