//Saya [Trisna Risnandar] Tidak melakukan kecurangan seperti yang telah di
//spesifikasikan pada mata kuliah PBO dalam mengerjakan 
//[KUIS], jika saya melakukan kecurangan maka Allah/Tuhan adalah
//saksi saya, dan saya bersedia menerima hukumanNya.
//Amin.


#include <conio.h>
#include <iostream>

using namespace std;
#include "mesin.cpp"

main(){

	//variabel untuk input
	int pembilang1, pembilang2;
	int penyebut1, penyebut2;

	//input pertama
	cout<<"Masukkan Pembilang dan Penyebut yang pertama \n";
	cout<<"Masukkan Pembilang : ";
	cin>>pembilang1;
	cout<<"Masukkan Penyebut : ";
	cin>>penyebut1;
	
	//input kedua
	cout<<"Masukkan Pembilang dan Penyebut yang kedua \n";
	cout<<"Masukkan Pembilang : ";
	cin>>pembilang2;
	cout<<"Masukkan Penyebut : ";
	cin>>penyebut2;

	//pemanggilan proses/prosedur
	prosesint(pembilang1, penyebut1, pembilang2, penyebut2);
	cout<<"\n";
	prosesdouble(pembilang1, penyebut1, pembilang2, penyebut2);

	return 0;
}